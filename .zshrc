export ZDOTS=$HOME/.config/zsh
export path=($HOME/.npm-global/bin $HOME/.local/bin/ $path)

source $ZDOTS/rc.zsh
