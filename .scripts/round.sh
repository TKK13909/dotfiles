#!/bin/bash

if test -f /tmp/round; then
	# Waybar
	sed -i 's/1rem/0rem/' ~/.config/waybar/style.css
	pkill waybar
	hyprctl dispatch exec waybar

	# Hyprland
	sed -i 's/rounding = 10/rounding = 0/' ~/.config/hypr/hyprland.conf

	# Wofi
	sed -i 's/1rem/0rem/' ~/.config/wofi/style.css

	# Swaync
	sed -i 's/1rem/0rem/' ~/.config/swaync/style.css
	pkill swaync
	hyprctl dispatch exec swaync

	rm /tmp/round
else
	# Waybar
	sed -i 's/0rem/1rem/' ~/.config/waybar/style.css
	pkill waybar
	hyprctl dispatch exec waybar

	# Hyprland
	sed -i 's/rounding = 0/rounding = 10/' ~/.config/hypr/hyprland.conf

	# Wofi
	sed -i 's/0rem/1rem/' ~/.config/wofi/style.css

	# Swaync
	sed -i 's/0rem/1rem/' ~/.config/swaync/style.css
	pkill swaync
	hyprctl dispatch exec swaync

	touch /tmp/round
fi
