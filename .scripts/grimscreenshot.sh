#!/bin/bash
SCREENSHOT=$HOME/pictures/screenshots/$(date +"%y-%m-%d_%H:%M:%S").png
grim -g "$(slurp)" "$SCREENSHOT"
gwenview $SCREENSHOT
