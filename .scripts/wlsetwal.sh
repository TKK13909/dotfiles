#!/bin/bash

WAL=$1
rm ~/.wlwal
echo $WAL >>~/.wlwal

if command -v "swaybg" >/dev/null 2>&1; then
	# Grey background flicker is prevented by killing old swaybg process after new one.
	# See https://github.com/swaywm/swaybg/issues/17#issuecomment-851680720
	PID=$(pidof swaybg)
	swaybg -i "$WAL" -m fill &
	if [ ! -z "$PID" ]; then
		sleep 1
		kill $PID 2>/dev/null
	fi
else
	swaymsg output "*" bg "$WP" fill
fi

cp $WAL /usr/share/sddm/themes/arcolinux-simplicity/images/background.png
#ffmpeg -i $WAL /boot/grub/themes/Vimix/background/background.png -y
