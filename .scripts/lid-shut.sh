#!/bin/bash

while true; do
    state=$(cat /proc/acpi/button/lid/LID0/state)
    if [[ $state == *"closed"* ]]; then
        echo Closed
        xset dpms force off
    else
        echo Open
        xset dpms force on
    fi
    sleep 0.2
done