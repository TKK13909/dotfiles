#!/bin/bash
OUT=$(ss -tun state connected | grep tcp | awk '{print $6}' | grep -o -E '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}')

declare -a LIST
LIST=($OUT)

#pkill firefox

#echo ${LIST[@]} | curl -XPOST --data-binary @- "https://ipinfo.io/tools/map?cli=1" | grep report | awk '{print $2}' | xargs firefox

for i in "${LIST[@]}"; do
	#sleep 1
	#ipinfo $i
	echo $i
	#echo $i | curl -XPOST --data-binary @- "https://ipinfo.io/tools/map?cli=1"
	geoiplookup $i -f /usr/share/GeoIP/GeoLiteCity.dat | grep -v not
	#geoiplookup $i | grep -v not
	#geoiplookup -f /usr/share/GeoIP/GeoLiteCity.dat $i | grep -v not | grep -o -E '[0-9]{1,3}\.[0-9]{1,6}|-[0-9]{1,3}\.[0-9]{1,6}'
done
