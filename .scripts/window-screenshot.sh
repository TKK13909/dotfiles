#!/bin/bash
SCREENSHOT=$HOME/pictures/screenshots/$(date +"%y-%m-%d_%H:%M:%S").png
ID=$(xdotool getwindowfocus)
echo $ID >>~/temp/temp.txt
import -window $ID $SCREENSHOT
gwenview $SCREENSHOT
