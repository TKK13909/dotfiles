#!/bin/bash
grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | grep 0,0 | awk '{print $2 $3}' | wl-copy
