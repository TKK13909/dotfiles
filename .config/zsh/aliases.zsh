
# Use "sudo showkey --scancodes" to get keys

bindkey "\e[3~" delete-char
bindkey "^[[H"  beginning-of-line
bindkey "^[[F"  end-of-line

bindkey '^[[A'  history-substring-search-up
bindkey '^[[B'  history-substring-search-down
bindkey '^[e'   history-substring-search-up
bindkey '^[o'   history-substring-search-down

bindkey '^[n'   vi-backward-char
bindkey '^[i'   vi-forward-char
bindkey '^[x'   delete-char
bindkey '^[z'   vi-backward-delete-char

function clear-scrollback-buffer {
  # Behavior of clear:
  # 1. clear scrollback if E3 cap is supported (terminal, platform specific)
  # 2. then clear visible screen
  # For some terminal 'e[3J' need to be sent explicitly to clear scrollback
  clear && printf '\e[3J'
  # .reset-prompt: bypass the zsh-syntax-highlighting wrapper
  # https://github.com/sorin-ionescu/prezto/issues/1026
  # https://github.com/zsh-users/zsh-autosuggestions/issues/107#issuecomment-183824034
  # -R: redisplay the prompt to avoid old prompts being eaten up
  # https://github.com/Powerlevel9k/powerlevel9k/pull/1176#discussion_r299303453
  zle && zle .reset-prompt && zle -R
}

zle -N clear-scrollback-buffer
bindkey '^L' clear-scrollback-buffer


# copy the active line from the command line buffer
# onto the system clipboard

copybuffer () { printf "%s" "$BUFFER" | tee >(wl-copy) | xclip -selection clipboard }
copydir () { pwd | xclip -selection clipboard }

zle -N copybuffer

bindkey -M emacs "^O" copybuffer
bindkey -M viins "^O" copybuffer
bindkey -M vicmd "^O" copybuffer


if [ $XDG_SESSION_TYPE = "wayland" ]; then
  alias cpf="wl-copy <"
  alias cpd="wl-copy $(pwd)"
else
  alias cpf="xclip -selection clipboard"
  alias cpd="pwd | xclip -selection clipboard"
fi

# Make directory and cd into it:
md () { mkdir -p "$@" && cd "$1"; }

alias history='history 1'
alias oldhist='wtype "$(cat $ZDOTS/oldhistory | fzf)"'

alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias ~='cd ~'
alias /='cd /'
alias zs='cd $ZDOTS'

alias d='dict -d gcide'
alias ntfy='~/.config/private/ntfy.sh'

alias ls='exa --icons --grid'
alias ls.='exa -d .*'
alias ll='exa -la --icons --git'
alias l='exa -lF --icons --git'
alias lc='exa --icons --group-directories-first -F'

alias RM='/bin/rm -rvf'
#alias rm='trash-put'
alias rm='rm -rv'
alias cp='cp -rv'
alias mv='mv -v'

alias sudo="sudo -A"
alias feh="imv"

alias catt='bat'
alias rsyncc="rsync --rsh='ssh -p96' --verbose --progress --update -r"
alias "kdeshare"="kdeconnect-cli -d 6bd40029_c8b0_4950_bb34_35d233cc656c"

alias v='nvim'
alias n='neovide'

alias sshs='~/.config/private/sshs.sh'
alias sshm='~/.config/private/sshm.sh'

alias sxrandr='xrandr --output VGA1 --auto --right-of LVDS1 && xrandr --output LVDS1 --auto'

alias connect='iwctl station wlan0 connect'

# Radio
RADIOFREEFEDI='http://37.58.57.168:8010/rff'
DEFRAG='https://www.youtube.com/watch?v=KR3TbL3Tl6M'
PLR='https://ice10.securenetsystems.net/PLR'
MUZAIKO='http://fluo.muzaiko.saluton.dk:8000/radio.mp3'

alias tune='mpv --no-video'

alias yayy='yay -Syyu --sudoflags -A'
#alias yay='yay --sudoflags -A'
alias code='vscodium'

alias config='/usr/bin/git --git-dir=$HOME/config --work-tree=$HOME'
