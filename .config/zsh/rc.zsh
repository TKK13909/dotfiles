#clear

export SUDO_ASKPASS=/usr/bin/sudo-askpass
export ZDOTS=$HOME/.config/zsh
export EDITOR=nvim
export QT_QPA_PLATFORM=wayland
export QT_QPA_PLATFORMTHEME="qt5ct"
fpath=($ZDOTS/completions $fpath)

autoload compinit
compinit -i

ENABLE_CORRECTION="true"
zle_highlight=('paste:none')

HISTFILE=$ZDOTS/history
HISTSIZE=100000
SAVEHIST=100000
setopt appendhistory
setopt HIST_IGNORE_SPACE

#SHELL="/bin/zsh"

#wal -i $(cat ~/.wlwal) >> /dev/null

# Use "sudo showkey --scancodes" to get keys

#bindkey "\e[A" history-search-backward

source $ZDOTS/aliases.zsh
source $ZDOTS/prompt.zsh
source $ZDOTS/plugins/calc.plugin.zsh
source $ZDOTS/plugins/zsh-autosuggestions.zsh
source $ZDOTS/plugins/sytax/syntax-highlighting.zsh
source $ZDOTS/plugins/zsh-history-substring-search.zsh
source $ZDOTS/plugins/dirhistory.plugin.zsh

plugins=(zsh-autosuggestions git)

autoload -Uz vcs_info

# Write some info to terminal title.
# This is seen when the shell prompts for input.
function precmd {
 print -Pn "\e]0;zsh(%L) %(1j,%j job%(2j|s|); ,)%~\a"
 vcs_info
 if [ $timer ]; then
  now=$(($(date +%s%0N)*0.000000001))
  elapsed=$(echo $(($now-$timer)) | awk '{printf "%.3f", $1}')

  export RPROMPT="%F{cyan}${elapsed}s %{$reset_color%}"
  unset timer
 fi
}
# Write command and args to terminal title.
# This is seen while the shell waits for a command to complete.
function preexec {
 printf "\033]0;%s\a" "$1"
 timer=$(($(date +%s%0N)*0.000000001))
}
