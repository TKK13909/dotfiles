setopt PROMPT_SUBST

zstyle ':vcs_info:git:*' formats $'%F{black}\U2646 %f%u%c'
zstyle ':vcs_info:*' check-for-changes true

NEWLINE=$'\n'

crt () { export PS1="┌%n [%~]${NEWLINE}└$ " }
simple () { export PROMPT="%F{3}λ " }
tokyo () { eval "$(starship init zsh)" }
normal () { export PROMPT=$'%F{5}%K{44}  \Ue0b6%F{0}%K{5}%n%F{5}%K{44}\Ue0b4 %F{0}%40<..<%~%<<% ${vcs_info_msg_0_}%F{44}%{%k%}\Ue0b4%{%k%} ' }

# Replace "tokyo" with any of the functions above to change the default prompt
tokyo
