-- Autocmds are automatically loaded on the VeryLazy event
-- Default autocmds that are always set: https://github.com/LazyVim/LazyVim/blob/main/lua/lazyvim/config/autocmds.lua
-- Add any additional autocmds here

--autocmd VimResized * wincmd=
--lua require'colorizer'.setup()
--ColorizerAttachToBuffer
--vim.cmd("setlocal spell spelllang=en_us")
vim.cmd("set guifont=Hack:h9")
