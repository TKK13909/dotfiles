monitor=,preferred,auto,auto


# Execute your favorite apps at launch
# exec-once = waybar & hyprpaper & firefox

# Source a file (multi-file configs)
# source = ~/.config/hypr/myColors.conf

# Some default env vars.
env = XCURSOR_SIZE,24

# Window rules

windowrule=opacity 1.0 override 0.9 override,^WebCord
windowrule=float, kcalc
windowrule=move 35% override 35% override,title:^Open Files
windowrule=maxsize 50 10,^(Spotify)$
#windowrule = float, nextcloud

layerrule = blur,           swaync-control-center
layerrule = ignorezero,     swaync-control-center

# For all categories, see https://wiki.hyprland.org/Configuring/Variables/
input {
    kb_layout = workman,us
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =

    follow_mouse = 1

    touchpad {
        natural_scroll = no
    }


    sensitivity = 0 # -1.0 - 1.0, 0 means no modification.
    numlock_by_default = true
}
misc {
  enable_swallow = true
  focus_on_activate = true
  swallow_regex = ^(Alacritty)$

  animate_mouse_windowdragging = true
}

general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 6
    gaps_out = 12
    border_size = 1
    col.active_border = rgb(8DAFF8)
    #col.active_border = rgba(1CBDD9ee) rgba(00ff99ee) 45deg
    bezier = linear, 0.0, 0.0, 1.0, 1.0
    #animation = borderangle, 1, 50, linear, loop
    col.inactive_border = rgba(3F4354aa)

    #cursor_inactive_timeout = 10

    layout = dwindle
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 10

      blur {
        enabled = true
        size = 5
        passes = 2
        xray = true
      }
    #blur = yes
    #blur_size = 5
    #blur_passes = 3
    #blur_new_optimizations = on

    drop_shadow = no
    shadow_range = 20
    shadow_render_power = 3
    col.shadow = rgba(8DAFF8aa)
    col.shadow_inactive = rgba(8DAFF800)
}

animations {
    enabled = yes

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05
    #bezier = md3_decel, 0.05, 0.7, 0.1, 1
    bezier = overshot,0.7,0.6,0.1,1.1
    bezier = normal,0.7,0.6,0.1,1
    bezier = swing, 0.860, -0.550, 0.070, 1.250

    animation = windows, 1, 5, myBezier, slide
    animation = windowsOut, 1, 7, overshot, slide
    animation = border, 1, 10, default
    animation = borderangle, 1, 50, linear, loop
    animation = fade, 1, 7, default
    animation = workspaces, 1, 6, swing, slide
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = yes # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
    new_is_master = true
}

gestures {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more
    workspace_swipe = on
}

# Example per-device config
# See https://wiki.hyprland.org/Configuring/Keywords/#executing for more
device:mosart-semi.-2.4g-wireless-mouse{
    input:sensitivity = 0.8
}
device:alpsps/2-alps-dualpoint-touchpad{
  input:sensitivity = 0.1
       }

# Example windowrule v1
# windowrule = float, ^(kitty)$
# Example windowrule v2
# windowrulev2 = float,class:^(kitty)$,title:^(kitty)$
# See https://wiki.hyprland.org/Configuring/Window-Rules/ for more

# See https://wiki.hyprland.org/Configuring/Keywords/ for more
$mainMod = SUPER

monitor=LVDS-1,1600x900,0x0,1
monitor=VGA-1,preferred,1600x0,1

bind = $mainMod, Return, exec, alacritty
bind = $mainMod SHIFT, Return, exec, alacritty -e bash ~/.config/private/sshs.sh

# Wofi
bind = $mainMod, D, exec, wofi --show drun
bind = $mainMod, W, exec, /home/tkk/.scripts/wlsetwal.sh $HOME/.wallpapers/$(tree -fi $HOME/.wallpapers/ | grep home | sed 's/\/home\/tkk\/.wallpapers//' | wofi --dmenu)
bind = $mainMod SHIFT, W, exec, pkill swaybg
bind = $mainMod, V, exec, mpvpaper $(hyprctl monitors | grep Monitor | awk '{print $2}' | wofi --dmenu) ~/.vidwall/$(/bin/ls ~/.vidwall/ | wofi --dmenu) -o --loop
bind = $mainMod SHIFT, V, exec, pkill mpvpaper

#bindr = ALT,ALT_L, exec, ydotool key 29:1 26:1 29:0 26:0

# Keyboard

bind = $mainMod, Prior, exec, hyprctl switchxkblayout at-translated-set-2-keyboard 0
bind = $mainMod, Next, exec, hyprctl switchxkblayout at-translated-set-2-keyboard 1

# General controls

bind = $mainMod, XF86PowerOff, exec, shutdown now

bind = $mainMod, A, exec, swaync-client -t -sw
bind = $mainMod, period, focusmonitor, VGA-1
bind = $mainMod, comma, focusmonitor, LVDS-1
bind = $mainMod, B, exec, waybar
bind = $mainMod SHIFT, B, exec, pkill waybar
bind = $mainMod, C, exec, alacritty --title 'Cava' -e cava
bind = $mainMod, F, fullscreen
bind = $mainMod, R, exec, ~/.scripts/round.sh
bind = $mainMod, L, exec, $HOME/.config/private/blr.sh
bind = $mainMod, M, exit,
bind = $mainMod SHIFT, P, pin
bind = $mainMod, Q, killactive,
bind = $mainMod, U, exec, alacritty --title 'System Update' -e yay -Syu --noconfirm && flatpak upgrade
bind = $mainMod, S, exec, touch /tmp/runwal
bind = $mainMod SHIFT, S, exec, rm /tmp/runwal
bind = $mainMod SHIFT, F, fakefullscreen
bind = $mainMod SHIFT, Space, togglefloating,
bind = $mainMod, P, pseudo, # dwindle
bind = $mainMod, J, togglesplit, # dwindle

# Media control

bind = , XF86AudioMute, exec, pactl set-sink-mute @DEFAULT_SINK@ toggle
binde = , XF86AudioLowerVolume, exec, pactl set-sink-volume @DEFAULT_SINK@ -5%
binde = , XF86AudioRaiseVolume, exec, pactl set-sink-volume @DEFAULT_SINK@ +5%

bind = $mainMod, F1, exec, pactl set-sink-mute @DEFAULT_SINK@ toggle
binde = $mainMod, F2, exec, pactl set-sink-volume @DEFAULT_SINK@ -5%
binde = $mainMod, F3, exec, pactl set-sink-volume @DEFAULT_SINK@ +5%
bind = $mainMod, F4, exec, pactl set-sink-volume @DEFAULT_SINK@ 100%

bind = $mainMod, F9, exec, kcalc
bind = $mainMod, F10, exec, playerctl previous
bind = $mainMod, F11, exec, playerctl play-pause
bind = $mainMod, F12, exec, playerctl next

bind = , XF86Calculator, exec, kcalc
bind = , XF86AudioPrev, exec, playerctl previous
bind = , XF86AudioPlay, exec, playerctl play-pause
bind = , XF86AudioNext, exec, playerctl next

# Capture

bind = , Print, exec, ~/.scripts/wlscreenshot.sh
bind = $mainMod, Print, exec, ~/.scripts/grimscreenshot.sh
bind = $mainMod SHIFT, C, exec, wl-color-picker

# Translate:

bind = ALT, C, exec, wtype ĉ
bind = ALT SHIFT, C, exec, wtype Ĉ
bind = ALT, G, exec, wtype ĝ
bind = ALT SHIFT, G, exec, wtype Ĝ
bind = ALT, H, exec, wtype ĥ
bind = ALT SHIFT, H, exec, wtype Ĥ
bind = ALT, J, exec, wtype ĵ
bind = ALT SHIFT, J, exec, wtype Ĵ
bind = ALT, S, exec, wtype ŝ
bind = ALT SHIFT, S, exec, wtype Ŝ
bind = ALT, U, exec, wtype ŭ
bind = ALT SHIFT, U, exec, wtype Ŭ

bind = $mainMod, T, exec, alacritty --title 'EN -> EO' -e /home/tkk/.local/bin/trans en:eo
bind = $mainMod SHIFT, T, exec, alacritty --title 'EO -> EN' -e /home/tkk/.local/bin/trans eo:en

# Brightness

binde = ,XF86MonBrightnessUp, exec, brightnessctl set 5%+
binde = ,XF86MonBrightnessDown, exec, brightnessctl set 5%-

# Move focus with mainMod + arrow keys
bind = $mainMod, left, movefocus, l
bind = $mainMod, right, movefocus, r
bind = $mainMod, up, movefocus, u
bind = $mainMod, down, movefocus, d

bind = $mainMod, N, movefocus, l
bind = $mainMod, I, movefocus, r
bind = $mainMod, E, movefocus, u
bind = $mainMod, O, movefocus, d

# Move window
bind = $mainMod SHIFT, left, movewindow, l
bind = $mainMod SHIFT, right, movewindow, r
bind = $mainMod SHIFT, up, movewindow, u
bind = $mainMod SHIFT, down, movewindow, d

bind = $mainMod SHIFT, N, movewindow, l
bind = $mainMod SHIFT, I, movewindow, r
bind = $mainMod SHIFT, E, movewindow, u
bind = $mainMod SHIFT, O, movewindow, d

binde = $mainMod CTRL SHIFT, N, moveactive, -20 0
binde = $mainMod CTRL SHIFT, I, moveactive, 20 0
binde = $mainMod CTRL SHIFT, E, moveactive, 0 -20
binde = $mainMod CTRL SHIFT, O, moveactive, 0 20

# Resize current window

binde = $mainMod CTRL, right, resizeactive, 1 0
binde = $mainMod CTRL, left, resizeactive, -1 0
binde = $mainMod CTRL, up, resizeactive, 0 -1
binde = $mainMod CTRL, down, resizeactive, 0 1

binde = $mainMod CTRL, I, resizeactive, 20 0
binde = $mainMod CTRL, N, resizeactive, -20 0
binde = $mainMod CTRL, E, resizeactive, 0 -20
binde = $mainMod CTRL, O, resizeactive, 0 20

# Bind Workspaces To Monitors

workspace=LVDS-1,1
workspace=LVDS-1,2
workspace=LVDS-1,3
workspace=LVDS-1,4
workspace=LVDS-1,5
workspace=LVDS-1,6
workspace=LVDS-1,7
workspace=LVDS-1,8
workspace=LVDS-1,9
workspace=LVDS-1,10

workspace=VGA-1,11
workspace=VGA-1,12
workspace=VGA-1,13
workspace=VGA-1,14
workspace=VGA-1,15
workspace=VGA-1,16
workspace=VGA-1,17
workspace=VGA-1,18
workspace=VGA-1,19
workspace=VGA-1,20

# Switch workspaces with mainMod + [0-9]

bind = $mainMod, bracketright, workspace, +1
bind = $mainMod, bracketleft, workspace, -1

bind = $mainMod SHIFT, bracketright, movetoworkspace, +1
bind = $mainMod SHIFT, bracketleft, movetoworkspace, -1

bind = $mainMod, 1, workspace, 1
bind = $mainMod, 2, workspace, 2
bind = $mainMod, 3, workspace, 3
bind = $mainMod, 4, workspace, 4
bind = $mainMod, 5, workspace, 5
bind = $mainMod, 6, workspace, 6
bind = $mainMod, 7, workspace, 7
bind = $mainMod, 8, workspace, 8
bind = $mainMod, 9, workspace, 9
bind = $mainMod, 0, workspace, 10

bind = $mainMod CTRL, 1, workspace, 11
bind = $mainMod CTRL, 2, workspace, 12
bind = $mainMod CTRL, 3, workspace, 13
bind = $mainMod CTRL, 4, workspace, 14
bind = $mainMod CTRL, 5, workspace, 15
bind = $mainMod CTRL, 6, workspace, 16
bind = $mainMod CTRL, 7, workspace, 17
bind = $mainMod CTRL, 8, workspace, 18
bind = $mainMod CTRL, 9, workspace, 19
bind = $mainMod CTRL, 0, workspace, 20

# Move active window to a workspace with mainMod + SHIFT + [0-9]
bind = $mainMod SHIFT, 1, movetoworkspacesilent, 1
bind = $mainMod SHIFT, 2, movetoworkspacesilent, 2
bind = $mainMod SHIFT, 3, movetoworkspacesilent, 3
bind = $mainMod SHIFT, 4, movetoworkspacesilent, 4
bind = $mainMod SHIFT, 5, movetoworkspacesilent, 5
bind = $mainMod SHIFT, 6, movetoworkspacesilent, 6
bind = $mainMod SHIFT, 7, movetoworkspacesilent, 7
bind = $mainMod SHIFT, 8, movetoworkspacesilent, 8
bind = $mainMod SHIFT, 9, movetoworkspacesilent, 9
bind = $mainMod SHIFT, 0, movetoworkspacesilent, 10

bind = $mainMod CTRL SHIFT, 1, movetoworkspacesilent, 11
bind = $mainMod CTRL SHIFT, 2, movetoworkspacesilent, 12
bind = $mainMod CTRL SHIFT, 3, movetoworkspacesilent, 13
bind = $mainMod CTRL SHIFT, 4, movetoworkspacesilent, 14
bind = $mainMod CTRL SHIFT, 5, movetoworkspacesilent, 15
bind = $mainMod CTRL SHIFT, 6, movetoworkspacesilent, 16
bind = $mainMod CTRL SHIFT, 7, movetoworkspacesilent, 17
bind = $mainMod CTRL SHIFT, 8, movetoworkspacesilent, 18
bind = $mainMod CTRL SHIFT, 9, movetoworkspacesilent, 19
bind = $mainMod CTRL SHIFT, 0, movetoworkspacesilent, 20

# Scroll through existing workspaces with mainMod + scroll
bind = $mainMod, mouse_down, workspace, e+1
bind = $mainMod, mouse_up, workspace, e-1

# Move/resize windows with mainMod + LMB/RMB and dragging
bindm = $mainMod, mouse:272, movewindow
bindm = $mainMod, mouse:273, resizewindow

# Open Folders

bind = $mainMod CTRL,code:90, exec, dolphin ~/documents/ --new-window
bind = $mainMod CTRL,code:87, exec, dolphin ~/internet/ --new-window
bind = $mainMod CTRL,code:88, exec, dolphin ~/music/ --new-window
bind = $mainMod CTRL,code:89, exec, dolphin ~/pictures/ --new-window
bind = $mainMod CTRL,code:83, exec, dolphin ~/videos/ --new-window
bind = $mainMod CTRL,code:84, exec, dolphin ~ --new-window
bind = $mainMod CTRL,code:85, exec, dolphin ~/apps/ --new-window
bind = $mainMod CTRL,code:79, exec, dolphin ~/git/ --new-window
bind = $mainMod CTRL,code:80, exec, dolphin ~/torrents/ --new-window
bind = $mainMod CTRL,code:81, exec, vscodium ~/school/ --new-window

# Startups

#env=QT_QPA_PLATFORM,wayland;xcb
#env=GDK_BACKEND,wayland,x11

#exec-once=dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP

exec-once = swaync
#exec-once = variety
exec-once = $HOME/.scripts/wlsetwal.sh $(cat $HOME/.wlwal)
exec-once = $HOME/.wallpapers/wal.sh
exec-once = waybar
exec-once = $HOME/.config/private/gammastep.sh
exec-once = ydotoold
exec-once = /usr/lib/kdeconnectd
exec-once = kdeconnect-indicator
#exec-once = nextcloud
exec-once = nm-applet
exec-once = blueman-applet

# Main Apps

bind = CTRL SHIFT, Y, exec, gtk-pipe-viewer
bind = CTRL SHIFT, M, exec, $HOME/apps/Fosstodon/Fosstodon
bind = CTRL SHIFT, G, exec, $HOME/apps/ProtonMail/ProtonMail

bind = CTRL,code:90, exec, steam
bind = CTRL,code:87, exec, multimc
bind = CTRL,code:88, exec, neovide
bind = CTRL,code:89, exec, alacritty --title btop -e btop
bind = CTRL,code:83, exec, alacritty --title musikcube -e musikcube
bind = CTRL,code:84, exec, nextcloud
bind = CTRL,code:85, exec, tor-browser
bind = CTRL,code:79, exec, librewolf
bind = CTRL,code:80, exec, webcord
bind = CTRL,code:81, exec, minetest

bind = CTRL,0,exec, steam
bind = CTRL,1,exec, multimc
bind = CTRL,2,exec, neovide
bind = CTRL,3,exec, alacritty --title btop -e btop
bind = CTRL,4,exec, alacritty --title musikcube -e musikcube
bind = CTRL,5,exec, nextcloud
bind = CTRL,6,exec, tor-browser
bind = CTRL,7,exec, librewolf
bind = CTRL,8,exec, webcord
bind = CTRL,9,exec, minetest
